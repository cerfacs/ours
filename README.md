# Projet Ours

Ce répertoire conserve les développement réalisés lors du projet visant à la
mise en oeuvre d'un dispositif d'effarouchement d'ours en vue de protéger les
estives ovines pyrénéennes de leur prédation. Ce projet, réalisé en 2021, a
impliqué la [DREAL Occitanie](http://www.occitanie.developpement-durable.gouv.fr/),
 et le [CERFACS](https://cerfacs.fr/).
